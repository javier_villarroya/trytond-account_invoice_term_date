# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.transaction import Transaction
from trytond.pool import PoolMeta
from trytond.modules.account_invoice.invoice import _STATES, _DEPENDS

__all__ = ['PaymentTerm', 'Invoice']
__metaclass__ = PoolMeta


class Invoice:
    __name__ = 'account.invoice'

    term_date = fields.Date('Payment Term Start Date', states=_STATES,
        depends=_DEPENDS, help=('The date used as start date to compute '
            'payment term maturity dates.'))

    def create_move(self):
        with Transaction().set_context(term_date=self.term_date):
            return super(Invoice, self).create_move()


class PaymentTerm:
    __name__ = 'account.invoice.payment_term'

    def compute(self, amount, currency, date=None):
        new_date = Transaction().context.get('term_date') or date
        return super(PaymentTerm, self).compute(amount, currency, new_date)
